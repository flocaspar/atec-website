<!doctype html>
<html lang="de">

<head>
  <meta charset="utf-8">
  <title>ATEC Armaturenbau und -Technik GmbH | Nehmen Sie Kontakt auf</title>
  <meta name="description" content="ATEC Armaturen und -Technik GmbH Ihr Partner für die Entwicklung und Herstellung totraumfreier Spezial-Kugelhähne in weich und metallisch dichtender Ausführung. Die ATEC Produktpalette deckt nahezu den kompletten Bedarf ab." />

  <meta property="og:type" content="website">
  <meta property="og:site_name" content="Atec Armaturenbau und Technik GmbH">
  <meta property="og:description" content="ATEC Armaturen und -Technik GmbH Ihr Partner für die Entwicklung und Herstellung totraumfreier Spezial-Kugelhähne in weich und metallisch dichtender Ausführung. Die ATEC Produktpalette deckt nahezu den kompletten Bedarf ab.">
  <meta property="og:image" content="/img/logo.png">
  <link rel="apple-touch-icon-precomposed" href="icons/apple-touch-icon-180x180.png" sizes="180x180">
  <link rel="apple-touch-icon-precomposed" href="icons/apple-touch-icon-167x167.png" sizes="167x167">
  <link rel="apple-touch-icon-precomposed" href="icons/apple-touch-icon-152x152.png" sizes="152x152">
  <link rel="apple-touch-icon-precomposed" href="icons/apple-touch-icon-120x120.png" sizes="120x120">
  <link rel="apple-touch-icon-precomposed" href="icons/apple-touch-icon-76x76.png" sizes="76x76">
  <link rel="apple-touch-icon-precomposed" href="icons/apple-touch-icon-60x60.png" sizes="60x60">
  <link rel="apple-touch-icon-precomposed" href="icons/apple-touch-icon.png">
  <link rel="shortcut icon" href="icons/apple-touch-icon.png">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
  <link href="css/themify-icons.css" rel="stylesheet" type="text/css" media="all" />
  <link href="css/flexslider.css" rel="stylesheet" type="text/css" media="all" />
  <link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
  <link href="css/ytplayer.css" rel="stylesheet" type="text/css" media="all" />
  <link href="css/theme-gunmetal.css" rel="stylesheet" type="text/css" media="all" />
  <link href="css/custom.css" rel="stylesheet" type="text/css" media="all" />
  <link href="fontawesome/css/all.css" rel="stylesheet">
  <script defer src="fontawesome/js/all.js"></script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TS3KN42');</script>
<!-- End Google Tag Manager -->

  <script>
    var gaProperty = 'UA-69726340-1';
    var disableStr = 'ga-disable-' + gaProperty;
    if (document.cookie.indexOf(disableStr + '=true') > -1) {
      window[disableStr] = true;
    }

    function gaOptout() {
      document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
      window[disableStr] = true;
      alert('Das Tracking ist jetzt deaktiviert');
    }
    (function(i, s, o, g, r, a, m) {
      i['GoogleAnalyticsObject'] = r;
      i[r] = i[r] || function() {
        (i[r].q = i[r].q || []).push(arguments)
      }, i[r].l = 1 * new Date();
      a = s.createElement(o),
        m = s.getElementsByTagName(o)[0];
      a.async = 1;
      a.src = g;
      m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-69726340-1', 'auto');
    ga('set', 'anonymizeIp', true);
    ga('send', 'pageview');
  </script>

</head>

<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TS3KN42"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
  <div class="nav-container">
    <nav>

      <div class="nav-bar">
        <div class="module left">
          <a href="index.php">
            <img class="logo logo-dark" alt="ATEC Armaturenbau und Technik GmbH" src="img/logo.png"></a>
          </a>
        </div>
        <div class="module widget-handle mobile-toggle right visible-sm visible-xs">
          <i class="ti-menu"></i>
        </div>
        <div class="module-group right">
          <div class="module left">

            <ul class="menu">
              <li class="has-dropdown">
                <a href="#">
                  Produkte
                </a>
                <ul>
                  <li>
                    <a href="bodenablass-kugelhahn.html">Bodenablass</a>
                  </li>
                  <li>
                    <a href="mehrwege-kugelhahn.html">Mehrwege</a>
                  </li>
                  <li>
                    <a href="durchgang-kugelhahn.html">Durchgang</a>
                  </li>
                  <li>
                    <a href="probenahme-kugelhahn.html">Probenahme</a>
                  </li>
                  <li>
                    <a href="segment-kugelhahn.html">Segment</a>
                  </li>
                  <li>
                    <a href="kompakt-kugelhahn.html">Kompakt</a>
                  </li>
                  <li>
                    <a href="metallisch-dichtender-kugelhahn.html">Metallisch dichtend</a>
                  </li>
                  <li>
                    <a href="sonderloesungen.html">Sonderlösungen</a>
                  </li>
                </ul>
              </li>

              <li class="has-dropdown">
                <a href="#">
                  Unternehmen
                </a>
                <ul>
                  <li>
                    <a href="atec-kurzprofil.html">Kurzprofil</a>
                  </li>
                  <li>
                    <a href="atec-philosophie.html">Philosophie</a>
                  </li>
                  <li>
                    <a href="atec-anwendungsfelder.html">Anwendungsfelder</a>
                  </li>
                  <li>
                    <a href="atec-referenzen.html">Referenzen</a>
                  </li>
                  <li>
                    <a href="atec-karriere.html">Karriere</a>
                  </li>
                </ul>
              </li>


              <li class="has-dropdown">
                <a href="#">
                  Leistungen
                </a>
                <ul>
                  <li>
                    <a href="konstruktion-entwicklung.html">Konstruktion &amp; Entwicklung</a>
                  </li>
                  <li>
                    <a href="produktion.html">Produktion</a>
                  </li>
                  <li>
                    <a href="reparatur-service.html">Reparatur &amp; Service</a>
                  </li>
                  <li>
                    <a href="automation.html">Automation</a>
                  </li>
                  <!--<li><a href="lohnfertigung.html">Lohnfertigung</a></li>-->
                </ul>
              </li>

              <li class="has-dropdown">
                <a href="#">
                  Media Center
                </a>
                <ul>
                  <li>
                    <a href="mediacenter-allgemein.html">Allgemein</a>
                  </li>
                  <li>
                    <a href="mediacenter-technische-datenblaetter.html">Techn. Datenblätter</a>
                  </li>
                  <li>
                    <a href="mediacenter-zertifikate.html">Zertifizierungen</a>
                  </li>
                  <!--<li><a href="mediacenter-videos.html">Videos</a></li>-->
                </ul>
              </li>
              <li class="has-dropdown">
                <a href="news.html">
                  News
                </a>
                <ul>
                  <li><a href="presse.html">Presse</a></li>
                </ul>
              </li>
              <li class="has-dropdown text-right">
                <a href="#">
                  Kontakt
                </a>
                <ul>
                  <li>
                    <a href="atec-kontakt.php">Anschrift</a>
                  </li>

                  <li>
                    <a href="https://goo.gl/maps/2Axwl" target="_blank">Anfahrt</a>
                  </li>
                  <li>
                    <a href="impressum.html">Impressum</a>
                  </li>
                  <!--<li><a href="sitemap.html">Sitemap</a></li>-->
                </ul>
              </li>
            </ul>
          </div>

          <div class="module widget-handle language left">
            <ul class="menu">
              <li class="has-dropdown text-right">
                <a href="/">DE</a>
                <ul>
                  <li>
                    <a href="eng/index.php">ENG</a>
                  </li>
                </ul>
              </li>
            </ul>

          </div>
        </div>

      </div>
    </nav>


  </div>

  <div class="main-container">




    <section class="image-bg overlay parallax pt112 pb112 pt-xs-80 pb-xs-80">
      <div class="background-image-holder">
        <img alt="image" class="background-image" src="img/atec-kontakt-head.jpg">
      </div>

      <div class="container">
        <div class="row">
          <div class="col-md-5 col-sm-6 col-md-push-7 col-sm-push-6">
            <h2 class="bold">Ihr Kontakt zu
              <br>ATEC Armaturenbau und<br />-Technik GmbH
              <br>
            </h2>
            <p class="lead mb48 mb-xs-32">ATEC Armaturenbau und -Technik GmbH
              <br>Raiffeisenstraße 29, 55270 Klein-Winternheim
              <br>Tel. +49 (0) 6136-76647-0, Fax +49 (0) 6136-76647-99
              <br>E-Mail: info@atec-armaturen.de</p>
          </div>
        </div>

      </div>

    </section>


    <section>
      <div class="container">
        <div class="row">
          <div class="col-sm-6 col-md-5">
            <h4 class="uppercase">Ihr Kontakt zu ATEC</h4>

            <p class="lead">
              Ihrem Partner für die Entwicklung und Herstellung totraumfreier Spezial-Kugelhähne in weich und metallisch dichtender Ausführung.
            </p>
            <hr>
            <p class="lead">
              <strong>ATEC Armaturenbau und -Technik GmbH</strong>
              <br> Raiffeisenstraße 29
              <br> 55270 Klein-Winternheim
            </p>
            <hr>
            <p class="lead">
              <strong>E-Mail:</strong>
              <a href="mailto:&#105;&#110;&#102;&#x6F;&#x40;&#97;&#116;&#101;&#x63;&#45;&#x61;&#x72;&#109;&#x61;&#116;&#x75;&#x72;&#101;&#x6E;&#x2E;&#x64;&#x65;">&#105;&#110;&#102;&#x6F;&#x40;&#97;&#116;&#101;&#x63;&#45;&#x61;&#x72;&#109;&#x61;&#116;&#x75;&#x72;&#101;&#x6E;&#x2E;&#x64;&#x65;</a>
              <br>
              <strong>Telefon</strong> +49 (0) 6136-76647-0
              <br>
              <strong>Fax</strong> +49 (0) 6136-76647-99
              <br>
            </p>
          </div>


          <div class="col-sm-6 col-md-5 col-md-offset-1">


            <?php

            $empfaenger = "info@atec-armaturen.de";
            $website = $_POST['website'];
            $name = $_POST['name'];
            $firma = $_POST['firma'];
            $email = $_POST['email'];
            $message = $_POST['message'];
            $p1 = $_POST['p1'];
            $p2 = $_POST['p2'];
            $p3 = $_POST['p3'];
            $p4 = $_POST['p4'];
            $p5 = $_POST['p5'];
            $p6 = $_POST['p6'];
            $p7 = $_POST['p7'];
            $dropdown = $_POST['dropdown'];


            $betreff = "Anfrage aus dem Kontaktformular";
            $text = "
	<html>
	<head>
	<title></title>
	</head>
	<body>
	<table width='500' border='0' cellspacing='0' cellpadding='0'>

		<tr>
		  <td width='210' height='25' valign='top'><strong>Name:</strong></td>
		  <td valign='top'>$name</td>
	  </tr>
		<tr>
		  <td width='210' height='25' valign='top'><strong>Firma:</strong></td>
		  <td valign='top'>$firma</td>
	  </tr>
		<tr>
		  <td width='210' height='25' valign='top'><strong>E-Mail:</strong></td>
		  <td valign='top'>$email</td>
	  </tr>
		<tr>
		  <td width='210' height='25' valign='top'><strong>Ich möchte Informationen zu:</strong></td>
		  <td valign='top'>$dropdown</td>
	  </tr>
	  <tr>
		  <td width='210' height='25' valign='top'>&nbsp;</td>
		  <td valign='top'>$p1</td>
	  </tr>
		<tr>
		  <td width='210' height='25' valign='top'>&nbsp;</td>
		  <td valign='top'>$p2</td>
	  </tr>
		<tr>
		  <td width='210' height='25' valign='top'>&nbsp;</td>
		  <td height='25' valign='top'>$p3</td>
	  </tr>
		<tr>
		  <td width='210' height='25' valign='top'>&nbsp;</td>
		  <td height='25' valign='top'>$p4</td>
	  </tr>
	   <tr>
		  <td width='210' height='25' valign='top'>&nbsp;</td>
		  <td height='25' valign='top'>$p5</td>
	  </tr>
	   <tr>
		  <td width='210' height='25' valign='top'>&nbsp;</td>
		  <td height='25' valign='top'>$p6</td>
	  </tr>
		<tr>
		  <td width='210' valign='top'><strong>Nachricht:</strong></td>
		  <td valign='top'>$message</td>
	  </tr>
	
	</table>
	</body>
	</html>
	";


            $headers  = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
            $headers .= "From: $email" . "\r\n";

            if (isset($_POST['an'])) {


              $fehler = false;

              if ($website) {
                $fehler .= "<div class='mail_wrong'>Dieses Feld muss leer bleiben.</div><br />";
              }


              if ($fehler) {
                echo $fehler;
              } else {




                mail($empfaenger, $betreff, $text, $headers);


                echo "<div class='mail_right'><strong>Vielen Dank $name.<br />Wir werden uns in Kürze bei Ihnen melden</strong></div><br />";
              }
            }

            /*
}
//gehröt zur obigen if abfrage 
*/


            /*
else if(isset($_POST['an']))
{
echo "<div class='mail_wrong'>Ihre E-Mail Adresse ist ungültig, bitte versuchen Sie es noch einmal.</div><br />";	
}
*/


            ?>


            <form class="form-email" action="atec-kontakt.php" method="post">

              <input type="text" class="validate-required" id="name" name="name" placeholder="Ihr Name *" />
              <input type="text" class="validate-required" id="firma" name="firma" placeholder="Firma *" />
              <input type="text" class="validate-required" id="email" name="email" placeholder="E-Mail Adresse *" />

              <div class="select-option">
                <i class="ti-angle-down"></i>
                <select name="dropdown">
                  <option selected value="Default">Informationen über</option>
                  <option value="Bodenablass" id="p1" name="p1">Bodenablass</option>
                  <option value="Mehrwege" id="p2" name="p2">Mehrwege</option>
                  <option value="Durchgang" id="p3" name="p3">Durchgang</option>
                  <option value="Probenahme" id="p4" name="p4">Probenahme</option>
                  <option value="Segment" id="p5" name="p5">Segment</option>
                  <option value="Metallisch dichtend" id="p6" name="p6">Metallisch dichtend</option>
                  <option value="Sonderlösungen" id="p7" name="p7">Sonderlösungen</option>
                </select>
              </div>
              <input type="text" id="website" name="website" class="hid_form" placeholder="Dieses Feld leer lassen" />
              <textarea class="validate-required" id="message" name="message" rows="2" placeholder="Nachricht"></textarea>

              <input type="checkbox" id="cbx" class="inp-cbx" style="display: none" required />
              <label for="cbx" class="cbx">
                <span>
                  <svg width="12px" height="10px" viewBox="0 0 12 10">
                    <polyline points="1.5 6 4.5 9 10.5 1"></polyline>
                  </svg>
                </span>
                <br>
                <span>
                  <b>Bitte akzeptieren Sie die Bedingungen um fortzufahren</b><br>
                  Sie erklären sich damit einverstanden, dass Ihre Daten zur Bearbeitung Ihres Anliegens verwendet werden. Weitere Informationen und Widerrufshinweise finden Sie in der Datenschutzerklärung. Eine Kopie Ihrer Nachricht wird and Ihre E-Mail-Adresse geschickt.
                </span>
                <br><br>
              </label>

              <input type="submit" id="an" name="an" value="Absenden" />
            </form>
          </div>
        </div>

      </div>
    </section>


    <section class="p0">
      <div class="map-holder pt180 pb180">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2567.727191753089!2d8.20898!3d49.94146!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47bd9170815969b7%3A0xf63cdd689b581d0!2sAtec+Armaturenbau+und+-Technik+GmbH!5e0!3m2!1sde!2sde!4v1439381897924"></iframe>
      </div>
    </section>





    <footer class="footer-2 bg-blue">
      <div class="container">
        <div class="row mb-xs-24">
          <div class="col-sm-4 col-md-3">
            <ul>
              <li>
                <h5 class="uppercase mb16 bold">ANSCHRIFT</h5>
              </li>
              <li>
                <h5 class="mb16">ATEC Armaturenbau und -Technik GmbH
                  <br> Raiffeisenstraße 29
                  <br> 55270 Klein-Winternheim</h5>
              </li>

               <li>
    <h5 class="mb16">Telefon <a href="tel:+49 (0) 6136-76647-0" class="mb16" style="color:#ffffff;"> +49 (0) 6136-76647-0 </a><p>Fax +49 (0) 6136-76647-99</p>
   </h5>
</li>

              <li><a href="mailto:&#105;&#110;&#102;&#x6F;&#x40;&#97;&#116;&#101;&#x63;&#45;&#x61;&#x72;&#109;&#x61;&#116;&#x75;&#x72;&#101;&#x6E;&#x2E;&#x64;&#x65;">
                  <h5 class="mb16 fade-on-hover">
                    &#105;&#110;&#102;&#x6F;&#x40;&#97;&#116;&#101;&#x63;&#45;&#x61;&#x72;&#109;&#x61;&#116;&#x75;&#x72;&#101;&#x6E;&#x2E;&#x64;&#x65;
                  </h5>
                </a></li>
            </ul>
          </div>

          <div class="col-sm-4 col-md-2">
            <ul>
              <li>
                <h5 class="uppercase mb16 bold">UNTERNEHMEN</h5>
              </li>
              <li><a href="atec-kurzprofil.html">
                  <h5 class="mb16 fade-on-hover">Kurzprofil</h5>
                </a></li>
              <li><a href="atec-philosophie.html">
                  <h5 class="mb16 fade-on-hover">Philosophie</h5>
                </a></li>
              <li><a href="atec-anwendungsfelder.html">
                  <h5 class="mb16 fade-on-hover">Anwendungsfelder</h5>
                </a></li>
              <li><a href="atec-referenzen.html">
                  <h5 class="mb16 fade-on-hover">Referenzen</h5>
                </a></li>
              <li><a href="atec-karriere.html">
                  <h5 class="mb16 fade-on-hover">Karriere</h5>
                </a></li>
            </ul>
          </div>
          <div class="col-sm-4 col-md-2">
            <ul>
              <li>
                <h5 class="uppercase mb16 bold">Leistungen</h5>
              </li>
              <li><a href="konstruktion-entwicklung.html">
                  <h5 class="mb16 fade-on-hover">Konstruktion &amp; Entwicklung</h5>
                </a></li>
              <li><a href="produktion.html">
                  <h5 class="mb16 fade-on-hover">Produktion</h5>
                </a></li>
              <li><a href="reparatur-service.html">
                  <h5 class="mb16 fade-on-hover">Reparaturen &amp; Service</h5>
                </a></li>
              <li><a href="automation.html">
                  <h5 class="mb16 fade-on-hover">Automation</h5>
                </a></li>
              <!-- <li><a href="lohnfertigung.html"><h5 class="mb16 fade-on-hover">Automation</h5></a></li>-->
            </ul>
          </div>
          <div class="col-sm-4 col-md-2">
            <ul>
              <li>
                <h5 class="uppercase mb16 bold">Produkte</h5>
              </li>
              <li><a href="bodenablass-kugelhahn.html">
                  <h5 class="mb16 fade-on-hover">Bodenablass<br></h5>
                </a></li>
              <li><a href="durchgang-kugelhahn.html">
                  <h5 class="mb16 fade-on-hover">2-Wege</h5>
                </a></li>
              <li><a href="mehrwege-kugelhahn.html">
                  <h5 class="mb16 fade-on-hover">3-Wege</h5>
                </a></li>
              <li><a href="probenahme-kugelhahn.html">
                  <h5 class="mb16 fade-on-hover">Probeentnahme</h5>
                </a></li>
              <li><a href="segment-kugelhahn.html">
                  <h5 class="mb16 fade-on-hover">Segment</h5>
                </a></li>
              <li><a href="kompakt-kugelhahn.html">
                  <h5 class="mb16 fade-on-hover">Kompakt</h5>
                </a></li>
              <li><a href="metallisch-dichtender-kugelhahn.html">
                  <h5 class="mb16 fade-on-hover">Metallisch gedichtet</h5>
                </a></li>
              <li><a href="sonderloesungen.html">
                  <h5 class="mb16 fade-on-hover">Sonderlösungen</h5>
                </a></li>
            </ul>
          </div>
          <div class="col-sm-4 col-md-3">
            <ul>
              <li>
                <h5 class="uppercase mb16 bold">Weiteres</h5>
              </li>
              <li><a href="atec-kontakt.php">
                  <h5 class="mb16 fade-on-hover">Kontakt</h5>
                </a></li>
              <li><a href="https://goo.gl/maps/1gD40">
                  <h5 class="mb16 fade-on-hover">Anfahrt</h5>
                </a></li>
              <li><a href="mediacenter-technische-datenblaetter.html">
                  <h5 class="mb16 fade-on-hover">Techn. Datenblätter<br></h5>
                </a></li>
              <li><a href="impressum.html">
                  <h5 class="mb16 fade-on-hover">Impressum</h5>
                </a></li>
              <li>
                <a href="datenschutz.html">
                  <h5 class="mb16 fade-on-hover">Datenschutz</h5>
                </a>
              </li>

            </ul>
          </div>


          <div class="col-sm-4"><span>&nbsp;</span></div>
          <div class="col-sm-3"><span>&nbsp;</span></div>
        </div>

        <div class="row">
          <div class="col-md-6 col-xs-12">
            <span>&copy; Copyright 2020 ATEC Armaturenbau und- Technik GmbH</span>
          </div>
          <div class="col-md-6 col-sm-4 col-xs-12">
          </div>
          <div class="col-md-2 col-sm-4 col-xs-12">
          </div>
          <div class="col-md-2 col-sm-4 col-xs-12">
          </div>
          <div class="col-md-2 col-sm-4 col-xs-12">
            <span class="social-icons">
              <a href="https://www.linkedin.com/company/atecarmaturenbau/" target="_blank">
                <i class="fab fa-linkedin"></i>
              </a>
              <a href="https://www.instagram.com/atecarmaturenbau/" target="_blank">
                <i class="fab fa-instagram"></i>
              </a>
              <a href="https://www.xing.com/profile/Andreas_Hampel21" target="_blank">
                <i class="fab fa-xing"></i>
              </a>
            </span>
          </div>
        </div>

      </div>
    </footer>
  </div>


  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/flexslider.min.js"></script>
  <script src="js/lightbox.min.js"></script>
  <script src="js/masonry.min.js"></script>
  <script src="js/twitterfetcher.min.js"></script>
  <script src="js/spectragram.min.js"></script>
  <script src="js/parallax.js"></script>
  <script src="js/scripts.js"></script>
  <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
  <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
  <script>
    window.addEventListener("load", function() {
      window.cookieconsent.initialise({
        "palette": {
          "popup": {
            "background": "#1F273D;"
          },
          "button": {
            "background": "#fff",
            "text": "#4c81c1"
          }
        },
        "position": "top",
        "static": true,
        "content": {
          "message": "Diese Website nutzt Cookies, um bestmögliche Funktionalität bieten zu können.",
          "dismiss": "Ok, verstanden",
          "link": "Mehr Infos",
          "href": "http://www.atec-armaturen.de/datenschutz.html"
        }
      })
    });
  </script>
</body>

</html>