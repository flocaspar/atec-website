<!DOCTYPE html>
<html lang="de">

<head>
  <meta charset="utf-8">
  <title>ATEC Armaturenbau und -Technik GmbH | Totraumfreie Spezial-Kugelhähne Entwicklung und Fabrikation</title>
  <meta name="robots" content="index,follow">
  <meta name="description"
    content="ATEC Armaturen und -Technik GmbH Ihr Partner für die Entwicklung und Herstellung totraumfreier Spezial-Kugelhähne in weich und metallisch dichtender Ausführung. Die ATEC Produktpalette deckt nahezu den kompletten Bedarf ab.">
  <meta name="keywords"
    content="Bodenablass Kugelhähne, Mehrwege Kugelhähne, Durchgangs Kugelhähne, Probenahme Kugelhähne, Segment Kugelhähne, Kompakt Kugelhahn, Metallisch gedichtete Kugelhähne, Sonderlösungen">
  <meta property="og:type" content="website">
  <meta property="og:site_name" content="Atec Armaturenbau und Technik GmbH">
  <meta property="og:title"
    content="ATEC Armaturenbau und -Technik GmbH | Totraumfreie Spezial-Kugelhähne Entwicklung und Fabrikation">
  <meta property="og:description"
    content="ATEC Armaturen und -Technik GmbH Ihr Partner für die Entwicklung und Herstellung totraumfreier Spezial-Kugelhähne in weich und metallisch dichtender Ausführung. Die ATEC Produktpalette deckt nahezu den kompletten Bedarf ab.">
  <meta property="og:image" content="/img/logo.png">
  <link rel="apple-touch-icon-precomposed" href="icons/apple-touch-icon-180x180.png" sizes="180x180">
  <link rel="apple-touch-icon-precomposed" href="icons/apple-touch-icon-167x167.png" sizes="167x167">
  <link rel="apple-touch-icon-precomposed" href="icons/apple-touch-icon-152x152.png" sizes="152x152">
  <link rel="apple-touch-icon-precomposed" href="icons/apple-touch-icon-120x120.png" sizes="120x120">
  <link rel="apple-touch-icon-precomposed" href="icons/apple-touch-icon-76x76.png" sizes="76x76">
  <link rel="apple-touch-icon-precomposed" href="icons/apple-touch-icon-60x60.png" sizes="60x60">
  <link rel="apple-touch-icon-precomposed" href="icons/apple-touch-icon.png">
  <link rel="shortcut icon" href="icons/apple-touch-icon.png">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
  <link href="css/themify-icons.css" rel="stylesheet" type="text/css" media="all" />
  <link href="css/flexslider.css" rel="stylesheet" type="text/css" media="all" />
  <link href="css/lightbox.min.css" rel="stylesheet" type="text/css" media="all" />
  <link href="css/ytplayer.css" rel="stylesheet" type="text/css" media="all" />
  <link href="css/theme-gunmetal.css" rel="stylesheet" media="all" />
  <link href="css/custom.css" rel="stylesheet" media="all" />
  <link href="fontawesome/css/all.css" rel="stylesheet">
  <script defer src="fontawesome/js/all.js"></script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TS3KN42');</script>
<!-- End Google Tag Manager -->

  <style>
  .inner-title-bloecke.hover-reveal .title,
  .inner-title.hover-reveal .title {
    opacity: 1;
    transform: translate3d(0, 0px, 0);
    -webkit-transform: translate3d(0, 0, 0);
    -moz-transform: translate3d(0, 0px, 0);
  }

  .inner-title-bloecke.hover-reveal .title>h5,
  .inner-title.hover-reveal .title>h5 {
    color: #1F273D;
  }
  </style>

  <script>
  var gaProperty = 'UA-69726340-1';
  var disableStr = 'ga-disable-' + gaProperty;
  if (document.cookie.indexOf(disableStr + '=true') > -1) {
    window[disableStr] = true;
  }

  function gaOptout() {
    document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
    window[disableStr] = true;
    alert('Das Tracking ist jetzt deaktiviert');
  }
  (function(i, s, o, g, r, a, m) {
    i['GoogleAnalyticsObject'] = r;
    i[r] = i[r] || function() {
      (i[r].q = i[r].q || []).push(arguments)
    }, i[r].l = 1 * new Date();
    a = s.createElement(o),
      m = s.getElementsByTagName(o)[0];
    a.async = 1;
    a.src = g;
    m.parentNode.insertBefore(a, m)
  })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');
  ga('create', 'UA-69726340-1', 'auto');
  ga('set', 'anonymizeIp', true);
  ga('send', 'pageview');
  </script>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
  <script>
  $(function() {
    $('a[href*=#]:not([href=#])').click(function() {
      if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this
        .hostname) {
        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
        if (target.length) {
          $('html,body').animate({
            scrollTop: target.offset().top
          }, 1000);
          return false;
        }
      }
    });
  });
  </script>
</head>

<body>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TS3KN42"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

  <div class="nav-container">

    <nav>
      <div class="nav-bar">
        <div class="module left">
          <a href="index.php">
            <img class="logo logo-dark" alt="ATEC Armaturenbau und Technik GmbH" src="img/logo.png">
          </a>
        </div>
        <div class="module widget-handle mobile-toggle right visible-sm visible-xs">
          <i class="ti-menu"></i>
        </div>
        <div class="module-group right">
          <div class="module left">

            <ul class="menu">
              <li class="has-dropdown">
                <a href="#">
                  Produkte
                </a>
                <ul>
                  <li><a href="bodenablass-kugelhahn.html">Bodenablass</a></li>
                  <li><a href="mehrwege-kugelhahn.html">Mehrwege</a></li>
                  <li><a href="durchgang-kugelhahn.html">Durchgang</a></li>
                  <li><a href="probenahme-kugelhahn.html">Probenahme</a></li>
                  <li><a href="segment-kugelhahn.html">Segment</a></li>
                  <li><a href="kompakt-kugelhahn.html">Kompakt</a></li>
                  <li><a href="metallisch-dichtender-kugelhahn.html">Metallisch dichtend</a></li>
                  <li><a href="sonderloesungen.html">Sonderlösungen</a></li>
                </ul>
              </li>

              <li class="has-dropdown">
                <a href="#">
                  Unternehmen
                </a>
                <ul>
                  <li><a href="atec-kurzprofil.html">Kurzprofil</a></li>
                  <li><a href="atec-philosophie.html">Philosophie</a></li>
                  <li><a href="atec-anwendungsfelder.html">Anwendungsfelder</a></li>
                  <li><a href="atec-referenzen.html">Referenzen</a></li>
                  <li><a href="atec-karriere.html">Karriere</a></li>
                </ul>
              </li>

              <li class="has-dropdown">
                <a href="#">
                  Leistungen
                </a>
                <ul>
                  <li><a href="konstruktion-entwicklung.html">Konstruktion &amp; Entwicklung</a></li>
                  <li><a href="produktion.html">Produktion</a></li>
                  <li><a href="reparatur-service.html">Reparatur &amp; Service</a></li>
                  <li><a href="automation.html">Automation</a></li>
                  <!--<li><a href="lohnfertigung.html">Lohnfertigung</a></li>-->
                </ul>
              </li>

              <li class="has-dropdown">
                <a href="#">
                  Media Center
                </a>
                <ul>
                  <li><a href="mediacenter-allgemein.html">Allgemein</a></li>
                  <li><a href="mediacenter-technische-datenblaetter.html">Techn. Datenblätter</a></li>
                  <li><a href="mediacenter-zertifikate.html">Zertifizierungen</a></li>
                  <!--<li><a href="mediacenter-videos.html">Videos</a></li>-->
                </ul>
              </li>
              <li class="has-dropdown">
                <a href="#">
                  News
                </a>
                <ul>
                  <li><a href="news.html">News</a></li>
                  <li><a href="presse.html">Presse</a></li>
                  <li><a href="produkt-des-monats.html">Produkt des Monats</a></li>
                </ul>
              </li>

              <li class="has-dropdown text-right">
                <a href="#">
                  Kontakt
                </a>
                <ul>
                  <li><a href="atec-kontakt.php">Anschrift</a></li>

                  <li><a href="https://goo.gl/maps/2Axwl" target="_blank">Anfahrt</a></li>
                  <li><a href="impressum.html">Impressum</a></li>
                  <!--<li><a href="sitemap.html">Sitemap</a></li>-->
                </ul>
              </li>
            </ul>
          </div>

          <div class="module widget-handle language left">
            <ul class="menu">
              <li class="has-dropdown text-right">
                <a href="/">DE</a>
                <ul>
                  <li>
                    <a href="eng/index.php">ENG</a>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        </div>

      </div>
    </nav>

  </div>

  <div class="main-container">

    <!-- Postit Telefon -->

    <!--  <div class="bf"><img class="holiday-de" src="https://www.atec-armaturen.de/img/ferien_2018.png"  style="position: absolute; top: 12%; right: 80%; display: block; left: 8%; z-index:5000!important;"></div> -->

    <a class="arrow-wrap hidden-xs hidden" href="#content">
      <span class="arrow"></span>
      <!--<span class="hint">scroll</span>-->
    </a>

    <section class="cover fullscreen image-slider slider-all-controls controls-inside parallax slider-full-height">

      <ul class="slides">


        <!-- <li class="overlay image-bg">
          <div class="background-image-holder">
            <img alt="image" class="background-image" src="img/atec-produktion-fraesen-verlauf.jpg">
          </div>
          <div class="container v-align-transform">
            <div class="row">
              <div class="col-md-6 col-sm-8">
                <h1 class="mb40 mb-xs-16 bold home-h1">ATEC - Spezial-Kugelhahn Hersteller<br></h1>
                <h6 class="uppercase mb16 bold">Auf Ihre Anforderungen zugeschnitten!</h6>
                <p class="lead mb40"> Ihr Partner für die Entwicklung und Herstellung von <br>
                  Sonder-Kugelhähnen in
                  weich und metallisch <br>
                  dichtenden Ausführungen</p>
                <a class="btn btn-lg" href="produktion.html">Erfahren Sie mehr.</a>
                <div id="scroll-mehr" class="demo">

                  <a href="#content"><span></span></a>
                  <span class="scroll-text">Scrollen um mehr zu erfahren</span>
                </div>
              </div>
            </div>
          </div>
        </li> -->

        <li class="overlay image-bg">
          <div class="background-image-holder">
            <img alt="image" class="background-image" src="img/entwicklung-produktionshalle.jpg">
            <!-- produktionsbild-atec-slider-2.jpg -->
            <!-- <img alt="image" class="background-image" src="img/index-spezial-kugelhahn-hersteller.jpg"> -->
          </div>
          <div class="container v-align-transform">
            <div class="row">
              <div class="col-md-6 col-sm-8">
                <h1 class="mb40 mb-xs-16 bold home-h1">ATEC - Spezial-Kugelhahn Hersteller<br></h1>
                <h6 class="uppercase mb16 bold">Auf Ihre Anforderungen zugeschnitten!</h6>
                <p class="lead mb40"> Ihr Partner für die Entwicklung und Herstellung von <br>
                  Sonder-Kugelhähnen in
                  weich und metallisch <br>
                  dichtenden Ausführungen</p>
                <a class="btn btn-lg" href="produktion.html">Erfahren Sie mehr.</a>
                <div id="scroll-mehr" class="demo">

                  <a href="#content"><span></span></a>
                  <span class="scroll-text">Scrollen um mehr zu erfahren</span>

                </div>
              </div>
            </div>
          </div>
        </li>

        <!-- if img comes in, delete inline style from image-bg here!!! -->
        <li class="image-bg">
          <div class="background-image-holder">
            <img alt="image" class="background-image" src="img/Atec_Anwendungsfelder.png">
          </div>
          <div class="container v-align-transform">
            <div class="row">
              <div class="col-md-6 col-sm-8">
                <h1 class="mb40 mb-xs-16 bold home-h1">ANWENDUNGS-<br />GEBIETE<br></h1>
                <p class="lead mb40" style="max-width: 300px;"> Unsere Spezial-Kugelhähne kommen an vielen Orten zum Einsatz</p>
                <a class="btn btn-lg" href="atec-anwendungsfelder.html">Erfahren Sie mehr.</a>
                <div id="scroll-mehr" class="demo">

                  <a href="#content"><span></span></a>
                  <span class="scroll-text">Scrollen um mehr zu erfahren</span>

                </div>
              </div>
            </div>
          </div>
        </li>

        <li class="overlay image-bg">
          <div class="background-image-holder">
            <img alt="image" class="background-image" src="img/exoten-slider.jpg"> <!-- exoten-slider.jpg -->
            <!-- Altes Img img/index-exotische-werkstoffe.jpg -->
          </div>
          <div class="container v-align-transform">
            <div class="row">
              <div class="col-md-6 col-sm-8">
                <h1 class="mb40 mb-xs-16 bold home-h1">Exotische Werkstoffe<br></h1>
                <h6 class="uppercase mb16 bold">Nach Kundenwunsch!</h6>
                <p class="lead mb40">Spezialist in der Fertigung von Armaturen aus Titan, Nickelbasislegierungen wie
                  Hastelloy&reg;,
                  Duplex und viele weitere austenitische, rostfreie Stähle sowie Hochtemperatur-Kunststoffe</p>
                <a class="btn btn-lg" href="atec-philosophie.html">Erfahren Sie mehr.</a>
                <div id="scroll-mehr" class="demo">

                  <a href="#content"><span></span></a>
                  <span class="scroll-text">Scrollen um mehr zu erfahren</span>

                </div>
              </div>
            </div>
          </div>
        </li>

        <li class="overlay image-bg">
          <div class="background-image-holder">
            <img alt="image" class="background-image" src="img/atec-kurzprofil-reparatur.jpg">
            <!-- Atec_MadeinGermany.png -->
          </div>
          <div class="container v-align-transform">
            <div class="row">
              <div class="col-md-6 col-sm-8">
                <h1 class="mb40 mb-xs-16 bold home-h1">100%<br>Made in Germany</h1>

                <p class="lead mb40">Entwickelt, konstruiert, gefertigt, montiert und geprüft im<br>
                  eigenen Haus in Klein-Winternheim bei Mainz</p>
                <a class="btn btn-lg" href="atec-philosophie.html">Erfahren Sie mehr.</a>
                <div id="scroll-mehr" class="demo">

                  <a href="#content"><span></span></a>
                  <span class="scroll-text">Scrollen um mehr zu erfahren</span>

                </div>
              </div>
            </div>
          </div>
        </li>

        <li class=" image-bg">
          <div class="background-image-holder">
            <img alt="image" class="background-image" src="img/Atec_Lieferzeit.png">
          </div>
          <div class="container v-align-transform">
            <div class="row">
              <div class="col-md-6 col-sm-8">
                <h1 class="mb40 mb-xs-16 bold home-h1">Kurze<br /> Lieferzeiten<br></h1>
                <h6 class="uppercase mb16 bold">Blitzlieferungen möglich!</h6>
                <p class="lead mb40">Wir legen großen Wert auf Eigenfertigung im eigenen Hause, <br>
                  um so flexibel wie möglich auf Kundenwünsche zu reagieren<br>
                  und kurze Lieferzeiten zu garantieren</p>
                <a class="btn btn-lg" href="atec-philosophie.html">Erfahren Sie mehr.</a>
                <div id="scroll-mehr" class="demo">

                  <a href="#content"><span></span></a>
                  <span class="scroll-text">Scrollen um mehr zu erfahren</span>

                </div>
              </div>
            </div>
          </div>
        </li>


        <li class="overlay overlay-heavy image-bg">
          <div class="background-image-holder">
            <img alt="image" class="background-image" src="img/index-totraumfrei.jpg">
          </div>
          <div class="container v-align-transform">
            <div class="row">
              <div class="col-md-6 col-sm-8">
                <h1 class="mb40 mb-xs-16 bold home-h1">Totraumfrei<br></h1>
                <h6 class="uppercase mb16 bold">Die Totraumfreiesten Kugelhähne auf dem Markt</h6>
                <p class="lead mb40">Keine Toträume zwischen Kugel und Gehäuse<br>
                  Kaum Zurückbleiben von Altprodukt im Kugelhahngehäuse<br>
                  Längere Standzeiten durch angefedertes Dichtsystem<br>
                  Annähernd gleiches Drehmoment bei unterschiedlichen Temperaturen</p>
                <a class="btn btn-lg" href="atec-philosophie.html">Erfahren Sie mehr.</a>
                <div id="scroll-mehr" class="demo">

                  <a href="#content"><span></span></a>
                  <span class="scroll-text">Scrollen um mehr zu erfahren</span>

                </div>
              </div>
            </div>
          </div>
        </li>
      </ul>
    </section>

    <!-- Messetermine erstmal auskommentiert, bis Korona Kriese durch ist. -->
    <!-- <section class="bg-blue" style="padding: 20px 0 0 0;">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-4 col-xs-12 text-left">
            <h2 class="black-uppercase HelveticaNowText-Black"
              style="color:white; font-size: 36px; letter-spacing: 1px; margin-top: 21px; line-height: 20px;">
              Messetermine</h2>
          </div>
          <div class="col-sm-12 col-md-4 col-xs-12">
            <p class="lead light" style="color:white; font-size:25px;">DIAM/DDM 2020<br />
              11. - 12. März 2020
            </p>
          </div>

          <div class="col-sm-12 col-md-4 col-xs-12">
            <p class="lead light" style="color:white; font-size:25px;">Valve World 2020 <br />
              01. - 03. Dezember 2020
            </p>
          </div>

        </div>
      </div>
    </section> -->
    <!-- End Messetermine -->

    <section class="bg-blue" style="padding: 20px 0 0 0;">
      <div class="container">
        <div class="row">
          <div class="col-sm-12 col-md-4 col-xs-12 text-left">
            <h2 class="black-uppercase HelveticaNowText-Black"
              style="color:white; font-size: 36px; letter-spacing: 1px; margin-top: 5px; line-height: 20px;">
              Termine</h2>
          </div>
          <div class="col-sm-12 col-md-8 col-xs-12">
            <!-- <p class="lead light" style="color:white; font-size:25px;">
              Aufgrund der aktuellen Situation <br />finden derzeit keine Termine statt.
            </p> -->
          </div>

          <!-- <div class="col-sm-12 col-md-4 col-xs-12">
            <p class="lead light" style="color:white; font-size:25px;">Valve World 2020 <br />
              01. - 03. Dezember 2020
            </p>
          </div> -->

        </div>
      </div>
    </section>


    <!-- Test Section -->
    <section style="background: rgba(31,39,61,0.5)" class="mobile-padding-top-null">
      <div class="container lg-same-height-container">
        <div class="row">

          <!-- Test -->
          <div class="col-md-4 mb16 lg-same-height" style="background: white; padding:0;">
            <div class="col-md-12" style="padding:0;">
              <!-- add bg-white class later -->
              <!-- <img src="img/test-real-teaser-news.jpg" alt="ATEC Armaturenbau News"> -->
              <img src="img/teaser-news.png" alt="ATEC Armaturenbau News">
              <div class="padding-xy-32">
                <h6 class="black-uppercase HelveticaNowText-Bold">News</h6>
                <p class="lead">Bleiben Sie immer auf dem Laufenden mit dem ATEC - Newsbereich</p>
                <a class="btn btn-sm btn-standard-blau" href="news.html">News <i class="fas fa-arrow-right"></i></a>
              </div>
            </div>
          </div>
          <div class="col-md-4 mb16 lg-same-height" style="background: white; padding:0;">
            <div class="col-md-12" style="padding:0;">
              <img src="img/kollage-test.png" alt="">
              <div class="padding-xy-32">

                <h6 class="black-uppercase HelveticaNowText-Bold">Produkt des Monats</h6>
                <p class="lead">Lesen Sie mehr über unser Produkt<br /> des Monats</p>
                <a class="btn btn-sm btn-standard-blau" href="produkt-des-monats.html">Mehr <i
                    class="fas fa-arrow-right"></i></a>
              </div>
            </div>

          </div>

          <!-- Produkt des Monats Backup -->
          <!-- <div class="col-md-4 mb16 lg-same-height" style="background: white; padding:0;">
            <div class="col-md-12" style="padding:0;">
              <img src="img/kollage-test.png" alt="">
              <div class="padding-xy-32">

                <h6 class="black-uppercase HelveticaNowText-Bold">Produkt des Monats<br></h6>
                <p class="lead mb0 bold atec-blue">3-Wege- Kugelhahn 120°- Ausführung</p>

                <p class="lead mb0 bold">» Nennweite DN40 PN16</p>
                <p class="lead mb0 bold">» Metall aus 1.4404</p>
                <p class="lead mb0 bold">» Angefedertes Dichtsystem</p>
                <p class="lead mb0 bold">» Totraumfrei</p>
                <p class="lead mb0 bold">» Leckrate A (gasdicht)</p>
                <p class="lead mb0 bold">» FDA- Konform</p>
                <p class="lead mb0 bold">» Spezielle Rohrbögen nach Kundenwunsch</p>
                <p class="lead bold">🇩🇪 100% „Made in Germany“ 🇩🇪</p>
              </div>
            </div>

          </div> -->

          <!-- End Produkt des Monats Backup -->

          <div class="col-md-4 mb16 lg-same-height" style="background: white; padding:0;">
            <div class="col-md-12" style="padding:0;">
              <!-- <img src="img/test-real-teaser-presse.jpg" alt="ATEC Armaturenbau Presse"> -->
              <img src="img/teaser-presse.png" alt="ATEC Armaturenbau News">
              <div class="padding-xy-32">
                <h6 class="black-uppercase HelveticaNowText-Bold">Presse</h6>
                <p class="lead">Lesen Sie mehr über unsere Produkte in unserem Pressebereich!</p>
                <a class="btn btn-sm btn-standard-blau" href="news.html">Presse <i class="fas fa-arrow-right"></i></a>
              </div>

            </div>
          </div>
          <!-- End Test -->

        </div>
        <!-- End Row -->

      </div>
    </section>
    <!-- End Test Section -->

    <section class="anwendungsfelder" style="background-color: rgba(31, 39, 61, 1);">
      <div class="container">
        <div class="row">

          <div class="col-md-12 text-center">
            <h3 class="uppercase text-white">Anwendungsfelder</h3>
          </div>
        </div>

        <div class="row">
          <!-- Test -->
          <div class="col-md-4 mb16">
            <div class="col-md-12">

              <div class="text-center pt16">
                <img src="img/icon-chemieindustrie.svg" alt="Chemieindustrie" width="70" style="margin-bottom:5px;">
                <h6 class="uppercase text-white">Chemieindustrie</h6>
                <p class="lead text-white">Säure &amp; Basen, Farbe, Lösungsmittel, Pulver, etc.</p>
              </div>
            </div>
          </div>
          <div class="col-md-4 mb16">
            <div class="col-md-12">
              <div class="text-center pt16">
                <img src="img/icon-spezialchemieindustrie.svg" alt="Spezialchemie" width="70"
                  style="margin-bottom:5px;">
                <h6 class="uppercase text-white">Spezialchemieindustrie
                  <br>
                </h6>
                <p class="lead text-white">Lacke, Kunststoffe, Kautschuk, etc.</p>
              </div>

            </div>
          </div>
          <!-- End Test -->

          <!-- Test -->
          <div class="col-md-4 mb16">
            <div class="col-md-12">

              <div class="text-center pt16">
                <img src="img/icon-pharmaindustrie.svg" alt="Chemieindustrie" width="70" style="margin-bottom:5px;">
                <h6 class="uppercase text-white">Pharmaindustrie</h6>
                <p class="lead text-white">Wirkstoffe, Impfstoffe, etc.</p>
              </div>
            </div>
          </div>
          <div class="col-md-4 mb16">
            <div class="col-md-12">
              <div class="text-center pt16">
                <img src="img/icon-agrarindustrie.svg" alt="Agrarindustrie" width="70" style="margin-bottom:5px;">
                <h6 class="uppercase text-white">Agrarindustrie
                  <br>
                </h6>
                <p class="lead text-white">Pestizide, Pflanzenschutz, Saatgut, etc.</p>
              </div>
            </div>
          </div>
          <!-- End Test -->
          <!-- Test -->
          <div class="col-md-4 mb16">
            <div class="col-md-12">

              <div class="text-center pt16">
                <img src="img/icon-nahrungsmittelindustrie.svg" alt="Nahrungsmittelindustrie" width="70"
                  style="margin-bottom:5px;">
                <h6 class="uppercase text-white">Nahrungsmittelindustrie</h6>
                <p class="lead text-white">Schokolade, Joghurt, Milch, etc.</p>
              </div>
            </div>
          </div>
          <div class="col-md-4 mb16">
            <div class="col-md-12">
              <div class="text-center pt16">
                <img src="img/icon-kosmetikindustrie.svg" alt="Spezialchemie" width="70" style="margin-bottom:5px;">
                <h6 class="uppercase text-white">Kosmetikindustrie
                  <br>
                </h6>
                <p class="lead text-white">Creme, Puder, Rouge, etc.</p>
              </div>

            </div>
          </div>
          <!-- End Test -->
        </div>

        <div class="row">
          <div class="container pt32">
            <div class="col-md-12 text-center">
              <a class="btn btn-lg btn-on-blau" style="margin-bottom: 15px"
                href="atec-anwendungsfelder.html">Anwendungsfelder <i class="fas fa-arrow-right"></i></a>
            </div>

          </div>
        </div>
      </div>
    </section>

    <!-- Produkte -->
    <section class="projects p0 bg-dark" id="content">
      <div class="row masonry masonryFlyIn">
        <div id="products"></div>

        <div class="col-sm-4 masonry-item project col-md-4" data-filter="Bodenablass">
          <div class="image-tile inner-title hover-reveal text-center">
            <a href="bodenablass-kugelhahn.html">
              <img alt="Bodenablass Kugelhahn" src="img/produktbild-bodenablass-kugelhahn.jpg">
              <div class="title">
                <img src="img/atec-icon-bodenablass.svg" alt="Bodenablass Kugelhahn" width="35" height="35"
                  style="width:35px!important;"><br>
                <h5 class="uppercase mb0">Bodenablass<br></h5>
                <h5 class="mb0">Kugelhahn</h5>
              </div>
            </a>
          </div>
        </div>

        <div class="col-sm-4 masonry-item project col-md-4" data-filter="Mehrwege">
          <div class="image-tile inner-title hover-reveal text-center">
            <a href="mehrwege-kugelhahn.html">
              <img alt="Mehrwege Kugelhahn" src="img/produktbild-mehrwege-kugelhahn.jpg">
              <div class="title">
                <img src="img/atec-icon-mehrwege.svg" alt="Mehrwege Kugelhahn" width="35" height="35"
                  style="width:35px!important;"><br>
                <h5 class="uppercase mb0">Mehrwege</h5>
                <h5 class="mb0">Kugelhahn</h5>
              </div>
            </a>
          </div>
        </div>

        <div class="col-sm-4 masonry-item project col-md-4" data-filter="Durchgangs">
          <div class="image-tile inner-title hover-reveal text-center">
            <a href="durchgang-kugelhahn.html">
              <img alt="Durchgangs Kugelhahn" src="img/produktbild-durchgang-kugelhahn.jpg">
              <div class="title">
                <img src="img/atec-icon-durchgang.svg" alt="Durchgangs Kugelhahn" width="35" height="35"
                  style="width:35px!important;"><br>
                <h5 class="uppercase mb0">Durchgangs</h5>
                <h5 class="mb0">Kugelhahn</h5>
              </div>
            </a>
          </div>
        </div>




        <div class="col-sm-4 masonry-item project col-md-4" data-filter="Segment">
          <div class="image-tile inner-title hover-reveal text-center">
            <a href="segment-kugelhahn.html">
              <img alt="Segment Kugelhahn" src="img/produktbild-segment-kugelhahn.jpg">
              <div class="title">
                <img src="img/atec-icon-segment.svg" alt="Segment Kugelhahn" width="35" height="35"
                  style="width:35px!important;"><br>
                <h5 class="uppercase mb0">Segment</h5>
                <h5 class="mb0">Kugelhahn</h5>
              </div>
            </a>
          </div>
        </div>

        <div class="col-sm-4 masonry-item project col-md-4" data-filter="Segment">
          <div class="image-tile inner-title hover-reveal text-center">
            <a href="kompakt-kugelhahn.html">
              <img alt="Kompakt Kugelhahn" src="img/produktbild-kompakt-kugelhahn.jpg">
              <div class="title">
                <img src="img/atec-icon-kompakt.svg" alt="Kompakt Kugelhahn" width="35" height="35"
                  style="width:35px!important;"><br>
                <h5 class="uppercase mb0">Kompakt</h5>
                <h5 class="mb0">Kugelhahn</h5>
              </div>
            </a>
          </div>
        </div>

        <div class="col-sm-4 masonry-item project col-md-4" data-filter="Probenahme">
          <div class="image-tile inner-title hover-reveal text-center">
            <a href="probenahme-kugelhahn.html">
              <img alt="Probenahme Kugelhahn" src="img/produktbild-probenahme-kugelhahn.jpg">
              <div class="title">
                <img src="img/atec-icon-probenahme.svg" alt="Probenahme Kugelhahn" width="55" height="55"
                  style="width:35px!important;"><br>
                <h5 class="uppercase mb0">Probenahme</h5>
                <h5 class="mb0">Kugelhahn</h5>
              </div>
            </a>
          </div>
        </div>

        <div class="col-sm-4 masonry-item project col-md-4" data-filter="Metallisch dichtender">
          <div class="image-tile inner-title hover-reveal text-center">
            <a href="metallisch-dichtender-kugelhahn.html">
              <img alt="Metallisch dichtender Kugelhahn" src="img/produktbild-metallisch-kugelhahn.jpg">
              <div class="title">
                <img src="img/atec-icon-metallisch.svg" alt="Bodenablass" width="35" height="35"
                  style="width:35px!important;"><br>
                <h5 class="uppercase mb0">Metallisch dichtender</h5>
                <h5 class="mb0">Kugelhahn</h5>
              </div>
            </a>
          </div>
        </div>

        <div class="col-sm-4 masonry-item project col-md-4" data-filter="Sonderlosungen">
          <div class="image-tile inner-title hover-reveal text-center">
            <a href="sonderloesungen.html">
              <img alt="Sonderlösungen" src="img/produktbild-sonderloesung-kugelhahn.jpg">
              <div class="title">
                <img src="img/atec-icon-eco.svg" alt="Bodenablass" width="35" height="35"
                  style="width:35px!important;"><br>
                <h5 class="uppercase mb0">Sonderlösungen</h5>
                <br>
              </div>
            </a>
          </div>
        </div>

        <!--
			<div class="col-sm-4 masonry-item project col-md-4" data-filter="Aktuelles">
			<div class="image-tile inner-title hover-reveal text-center">
			<a href="atec-news.html">
			<img alt="Aktuelles" src="img/news_xmas_2017_start.jpg">
			<div class="title">
			<img src="img/atec-icon-news.png" alt="Frohe Weihnachten" width="35" height="35" style="width:35px!important;"><br>
			<h5 class="uppercase mb0 bold">Aktuelles</h5> <h5 class="mb0">aus unserem Hause</h5>
			</div>
			</a>
			</div>
			</div>
			-->
        <!-- <div class="col-sm-4 masonry-item project col-md-4" data-filter="Aktuelles">
					<div class="image-tile inner-title hover-reveal text-center">
						<a href="atec-news.html">
							<img alt="Aktuelles" src="img/atec_nl_02_18_v2.jpg">
							<div class="title">
								<img src="img/atec-icon-news.png" alt="Totraumfreie Kugelhähne" width="35" height="35"
									style="width:35px!important;"><br>
								<h5 class="uppercase mb0 bold">Aktuelles</h5>
								<h5 class="mb0">aus unserem Hause</h5>
							</div>
						</a>
					</div>
				</div> -->

      </div>
    </section>

    <section>
      <div class="container">
        <div class="row text-center">
          <div class="col-md-9 mb30">
          </div>
        </div>

        <div class="row mb40 mb-xs-0 text-center">
          <div class="col-md-12">
            <p class="lead" style="color:#444;">Die ATEC Armaturenbau und -Technik GmbH ist Spezialist f&uuml;r
              totraumfreie Kugelh&auml;hne in weich- und metallisch dichtender Ausf&uuml;hrung sowie Sonderl&ouml;sungen
              f&uuml;r anspruchsvollste Bedingungen. Die Entwicklung und Konstruktion unserer Produkte findet komplett
              im eigenen Hause statt, was es uns erm&ouml;glicht, schnell und flexibel auf Kundenw&uuml;nsche zu
              reagieren. ATEC-Spezialkugelh&auml;hne sind ausgelegt, um h&ouml;chste Anforderungen in den Bereichen
              chemische und Nahrungsmittelindustrie, Pharmazie und Kosmetik zu erf&uuml;llen. Die enge Zusammenarbeit
              mit dem Kunden bildet die Grundlage f&uuml;r ein vertrauensvolles, partnerschaftliches Verh&auml;ltnis.
              Eine hohe Flexibilit&auml;t sowie das innovative Denken zeichnen unser Unternehmen aus und sorgen
              daf&uuml;r, dass der Kunde stets von einer optimalen L&ouml;sung profitiert. Unsere ausgefeilte Logistik
              erm&ouml;glicht au&szlig;erdem die schnelle Lieferung von Sonder-Kugelh&auml;hnen und Ersatzteilen, sodass
              der Kunde im Bedarfsfall rechtzeitig bedient wird.</p>
            <h4>IHR NUTZEN:</h4>
            <p class="lead mb0 bold">KURZE LIEFERZEITEN</p>
            <p class="lead mb0 bold">SONDER WERKSTOFFE</p>
            <p class="lead mb0 bold">SONDER KONSTRUKTIONEN</p>
            <p class="lead mb0 bold">L&Auml;NGERE STANDZEITEN </p>
            <p class="lead mb0 bold">STEIGERUNG DER WIRTSCHAFTLICHKEIT</p>
            <p class="lead mb0 bold">SCHNELLE ERSATZTEILLIEFERUNG</p>
            <p class="lead mb0 bold">SCHNELLE REAKTIONSZEIT BEI AUSFÄLLEN</p>
          </div>

        </div>
      </div>
    </section>
    <!--
							<section class="pb0">
				<div class="container">
				<div class="row">

				<div class="col-md-16 text-center">
				<h6 class="uppercase mb8 mb-xs-8">Aktueller Newsletter</h6>
				<h3>Bodenablass-Kugelhahn</h3>

				<div class="container">
				<div class="row">

				<div class="col-md-16 col-sm-16 col-xs-12">
				</div>

				<div class="col-md-16 col-sm-16 col-xs-12">
				<img src="img/news_bodenablass_kh.jpg" alt="Bodenablass-Kugelhahn News">
				</div>

				<div class="col-md-4 col-sm-4 col-xs-12">
				</div>

				</div>
				</div>
				<div class="col-md-9 text-center">
				<br>
				<p class="lead pb0">die ATEC Armaturenbau und -Technik GmbH fertigt hochwertige totraumfreie <strong>Bodenablass-Kugelhähne</strong><br>
					für den Einsatz in Rührbehältern in weich und metallisch dichtender Ausführung speziell nach Kundenwunsch.</p>
				<p class="lead bold">Folgende Vorteile bietet ein ATEC Bodenablass-Kugelhahn:</p>
				<p class="lead mb0">› Totraumfrei<br />
				› Sumpfarmer Einbau in den Behälter-Blockflansch<br />
				› Sonderbaugrößen für spezielle Kunden-Blockflansche<br />
				› Voller Durchgang<br />
				› Schräg abgehende Schaltwelle<br />
				› Doppelte Blockfl anschabdichtung<br />
				› Angefedertes Kugel-Dichtsystem<br />
				› Ausblassichere Schaltwelle<br /></p>
				<p class="lead">Wir produzieren alle Kugelhähne und Ersatzteile im Haus, um so flexibel wie möglich auf Kundenwünsche zu reagieren<br>
					und ermöglichen damit sehr kurze Lieferzeiten von Sonderanfertigungen.</p>
				<p class="lead bold">An unseren Maschinen werden nur Edelstähle (oder höherwertig) und Hochtemperaturkunststoffe verarbeitet. </p>

				<p class="lead"><a href="https://www.atec-armaturen.de/bodenablass-kugelhahn.html"  style="color: #c40000;">HIER ERFAHREN SIE MEHR</a></p>
					</div>
				</div>

				</div>
				</div>	        
				</section>		-->

    <!-- <section>
			<div class="container">
				<div class="row">
					<div class="col-sm-8">
						<h4 class="thin">Produkt des Monats:</h4>

						<div class="image-tile inner-title prOverview"><img alt="Pic" src="img/kollage.jpg">
						</div>

						<p class="lead mb0 bold blau">3-Wege- Kugelhahn 120°- Ausführung</p>

						<p class="lead mb0 bold">» Nennweite DN40 PN16</p>

						<p class="lead mb0 bold">» Metall aus 1.4404</p>

						<p class="lead mb0 bold">» Angefedertes Dichtsystem</p>

						<p class="lead mb0 bold">» Totraumfrei</p>

						<p class="lead mb0 bold">» Leckrate A (gasdicht)</p>

						<p class="lead mb0 bold">» FDA- Konform</p>

						<p class="lead mb0 bold">» Spezielle Rohrbögen nach Kundenwunsch</p>

						<p class="lead mb0 bold">» 100 % „Made in Germany“</p>
					</div>

				</div>
			</div>
	</div>
	</section> -->

    <footer class="footer-2 bg-blue">
      <div class="container">
        <div class="row mb-xs-24">
          <div class="col-sm-4 col-md-3">
            <ul>
              <li>
                <h5 class="uppercase mb16 bold">ANSCHRIFT</h5>
              </li>
              <li>
                <h5 class="mb16">ATEC Armaturenbau und -Technik GmbH
                  <br> Raiffeisenstraße 29
                  <br> 55270 Klein-Winternheim</h5>
              </li>

              <li>
    <h5 class="mb16">Telefon <a href="tel:+49 (0) 6136-76647-0" class="mb16" style="color:#ffffff;"> +49 (0) 6136-76647-0 </a><p>Fax +49 (0) 6136-76647-99</p>
   </h5>
</li>
              

              <li><a
                  href="mailto:&#105;&#110;&#102;&#x6F;&#x40;&#97;&#116;&#101;&#x63;&#45;&#x61;&#x72;&#109;&#x61;&#116;&#x75;&#x72;&#101;&#x6E;&#x2E;&#x64;&#x65;">
                  <h5 class="mb16 fade-on-hover">
                    &#105;&#110;&#102;&#x6F;&#x40;&#97;&#116;&#101;&#x63;&#45;&#x61;&#x72;&#109;&#x61;&#116;&#x75;&#x72;&#101;&#x6E;&#x2E;&#x64;&#x65;
                  </h5>
                </a></li>
            </ul>
          </div>

          <div class="col-sm-4 col-md-2">
            <ul>
              <li>
                <h5 class="uppercase mb16 bold">UNTERNEHMEN</h5>
              </li>
              <li><a href="atec-kurzprofil.html">
                  <h5 class="mb16 fade-on-hover">Kurzprofil</h5>
                </a></li>
              <li><a href="atec-philosophie.html">
                  <h5 class="mb16 fade-on-hover">Philosophie</h5>
                </a></li>
              <li><a href="atec-anwendungsfelder.html">
                  <h5 class="mb16 fade-on-hover">Anwendungsfelder</h5>
                </a></li>
              <li><a href="atec-referenzen.html">
                  <h5 class="mb16 fade-on-hover">Referenzen</h5>
                </a></li>
              <li><a href="atec-karriere.html">
                  <h5 class="mb16 fade-on-hover">Karriere</h5>
                </a></li>
            </ul>
          </div>
          <div class="col-sm-4 col-md-2">
            <ul>
              <li>
                <h5 class="uppercase mb16 bold">Leistungen</h5>
              </li>
              <li><a href="konstruktion-entwicklung.html">
                  <h5 class="mb16 fade-on-hover">Konstruktion &amp; Entwicklung</h5>
                </a></li>
              <li><a href="produktion.html">
                  <h5 class="mb16 fade-on-hover">Produktion</h5>
                </a></li>
              <li><a href="reparatur-service.html">
                  <h5 class="mb16 fade-on-hover">Reparaturen &amp; Service</h5>
                </a></li>
              <li><a href="automation.html">
                  <h5 class="mb16 fade-on-hover">Automation</h5>
                </a></li>
              <!-- <li><a href="lohnfertigung.html"><h5 class="mb16 fade-on-hover">Automation</h5></a></li>-->
            </ul>
          </div>
          <div class="col-sm-4 col-md-2">
            <ul>
              <li>
                <h5 class="uppercase mb16 bold">Produkte</h5>
              </li>
              <li><a href="bodenablass-kugelhahn.html">
                  <h5 class="mb16 fade-on-hover">Bodenablass<br></h5>
                </a></li>
              <li><a href="durchgang-kugelhahn.html">
                  <h5 class="mb16 fade-on-hover">2-Wege</h5>
                </a></li>
              <li><a href="mehrwege-kugelhahn.html">
                  <h5 class="mb16 fade-on-hover">3-Wege</h5>
                </a></li>
              <li><a href="probenahme-kugelhahn.html">
                  <h5 class="mb16 fade-on-hover">Probeentnahme</h5>
                </a></li>
              <li><a href="segment-kugelhahn.html">
                  <h5 class="mb16 fade-on-hover">Segment</h5>
                </a></li>
              <li><a href="kompakt-kugelhahn.html">
                  <h5 class="mb16 fade-on-hover">Kompakt</h5>
                </a></li>
              <li><a href="metallisch-dichtender-kugelhahn.html">
                  <h5 class="mb16 fade-on-hover">Metallisch gedichtet</h5>
                </a></li>
              <li><a href="sonderloesungen.html">
                  <h5 class="mb16 fade-on-hover">Sonderlösungen</h5>
                </a></li>
            </ul>
          </div>
          <div class="col-sm-4 col-md-3">
            <ul>
              <li>
                <h5 class="uppercase mb16 bold">Weiteres</h5>
              </li>
              <li><a href="atec-kontakt.php">
                  <h5 class="mb16 fade-on-hover">Kontakt</h5>
                </a></li>
              <li><a href="https://goo.gl/maps/1gD40">
                  <h5 class="mb16 fade-on-hover">Anfahrt</h5>
                </a></li>
              <li><a href="mediacenter-technische-datenblaetter.html">
                  <h5 class="mb16 fade-on-hover">Techn. Datenblätter<br></h5>
                </a></li>
              <li><a href="impressum.html">
                  <h5 class="mb16 fade-on-hover">Impressum</h5>
                </a></li>
              <li>
                <a href="datenschutz.html">
                  <h5 class="mb16 fade-on-hover">Datenschutz</h5>
                </a>
              </li>

            </ul>
          </div>


          <div class="col-sm-4"><span>&nbsp;</span></div>
          <div class="col-sm-3"><span>&nbsp;</span></div>
        </div>

        <div class="row">
          <div class="col-md-6 col-xs-12">
            <span>&copy; Copyright 2020 ATEC Armaturenbau und- Technik GmbH</span>
          </div>
          <div class="col-md-6 col-sm-4 col-xs-12">
          </div>
          <div class="col-md-2 col-sm-4 col-xs-12">
          </div>
          <div class="col-md-2 col-sm-4 col-xs-12">
          </div>
          <div class="col-md-2 col-sm-4 col-xs-12">
            <span class="social-icons">
              <a href="https://www.linkedin.com/company/atecarmaturenbau/" target="_blank">
                <i class="fab fa-linkedin"></i>
              </a>
              <a href="https://www.instagram.com/atecarmaturenbau/" target="_blank">
                <i class="fab fa-instagram"></i>
              </a>
              <a href="https://www.xing.com/profile/Andreas_Hampel21" target="_blank">
                <i class="fab fa-xing"></i>
              </a>
            </span>
          </div>
        </div>

      </div>
    </footer>
  </div>

  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/flexslider.min.js"></script>
  <script src="js/lightbox.min.js"></script>
  <script src="js/masonry.min.js"></script>
  <script src="js/twitterfetcher.min.js"></script>
  <script src="js/spectragram.min.js"></script>
  <script src="js/parallax.js"></script>
  <script src="js/scripts.js"></script>
  <link rel="stylesheet" type="text/css"
    href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
  <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>
  <script>
  window.addEventListener("load", function() {
    window.cookieconsent.initialise({
      "palette": {
        "popup": {
          "background": "#1F273D"
        },
        "button": {
          "background": "#fff",
          "text": "#1F273D"
        }
      },
      "position": "top",
      "static": true,
      "content": {
        "message": "Diese Website nutzt Cookies, um bestmögliche Funktionalität bieten zu können.",
        "dismiss": "Ok, verstanden",
        "link": "Mehr Infos",
        "href": "http://www.atec-armaturen.de/datenschutz.html"
      }
    })
  });
  </script>
</body>

</html>